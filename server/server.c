#include "head.h"
#include "server_func.h"
int exitPipe[2];
void sigFunc(int signum){
    printf("signum = %d\n", signum);
    write(exitPipe[1],"1",1);
    printf("Parent process is going to die!\n");
}
int main(int argc, char *argv[]){
    // ./server 192.168.152.130 1234 3
    // ./NetDisk
    ARGS_CHECK(argc,4);
    pipe(exitPipe);
    printf("main process pid = %d\n",getpid());
    if(fork() != 0){
        close(exitPipe[0]);
        signal(SIGUSR1,sigFunc);
        wait(NULL);
        exit(0);
    }
    chdir("./NetDisk");
    close(exitPipe[1]);
    int workerNum = atoi(argv[3]);
    threadPool_t threadPool;
    threadPoolInit(&threadPool,workerNum);
    makeWorker(&threadPool);
    int sockFd;
    tcpInit(&sockFd,argv[1],argv[2]);
    int epfd = epoll_create(1);
    epollAdd(sockFd,epfd);
    epollAdd(exitPipe[0],epfd);
    struct epoll_event readyArr[2];
    while(1){
        int readyNum = epoll_wait(epfd,readyArr,2,-1);
        printf("epoll_wait returns\n");
        for(int i = 0; i < readyNum; ++i){
            if(readyArr[i].data.fd == sockFd){
                
                int netFd = accept(sockFd,NULL,NULL);
                
                pthread_mutex_lock(&threadPool.taskQueue.mutex);
                taskEnqueue(&threadPool.taskQueue,netFd);
                printf("New task!\n");
                pthread_cond_signal(&threadPool.taskQueue.cond);
                pthread_mutex_unlock(&threadPool.taskQueue.mutex);
            }
            else if(readyArr[i].data.fd == exitPipe[0]){
                printf("child process, threadPool is going to die\n");
                threadPool.exitFlag = 1;
                pthread_cond_broadcast(&threadPool.taskQueue.cond);
                for(int j = 0;j < workerNum; ++j){
                    pthread_join(threadPool.tid[j],NULL);
                }
                pthread_exit(NULL);
            }
        }
    } 
    
}
int threadPoolInit(threadPool_t *pThreadPool, int workerNum){
    pThreadPool->threadNum = workerNum;
    pThreadPool->tid = (pthread_t *)calloc(workerNum,sizeof(pthread_t));
    pThreadPool->taskQueue.pFront = NULL;
    pThreadPool->taskQueue.pRear = NULL;
    pThreadPool->taskQueue.size = 0;
    pthread_mutex_init(&pThreadPool->taskQueue.mutex,NULL);
    pthread_cond_init(&pThreadPool->taskQueue.cond,NULL);
    pThreadPool->exitFlag = 0;
}