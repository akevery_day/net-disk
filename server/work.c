#include "server_func.h"
//实现客户端与服务端的交互
int interact_with_client(int netfd)
{    
    train_t msg;
    bzero(&msg,sizeof(msg));
    int ret;
    char username[1024]={0};
    ret=recvn(netfd,&msg.length,sizeof(int));
    if(ret==-1) return -1;
    switch (msg.length)
    {
    case 0:
        ret=client_register(netfd);
        if(ret==-1) return -1;
        ret=client_log(netfd,username);
        if(ret==-1) return -1;
        break;
    case 1:
        ret=client_log(netfd,username);
        if(ret==-1) return -1;
        break;
    default:
        return 0;
        break;
    }

    MYSQL *conn =NULL;
    char *host = "localhost";
    char *user = "root";
    char *password = "123456";
    char * db ="FileServer";
    conn = mysql_init(NULL);
    mysql_real_connect(conn,host,user,password,db,0,NULL,0);

    while(1)
    {
        bzero(&msg,sizeof(msg));
        recvn(netfd,&msg.length,sizeof(int));
        //printf("%d\n",msg.length);
        if(msg.length == 0) break;
        recvn(netfd,msg.buf,msg.length);
        //printf("%s\n",msg.buf);
        char* str[2];
        bzero(str,sizeof(str));
        char *pSave = NULL;
        str[0] = __strtok_r(msg.buf," ",&pSave);
        str[1] = __strtok_r(NULL," ",&pSave);
        
        if(strcmp(str[0],"cd")==0){
            //printf("%s %s\n",str[0],str[1]);
            change_directory(str[1],netfd,conn,username);
        }
        else if(strcmp(str[0],"pwd")==0){
            present_work_directory(netfd,conn,username);
        }
        else if(strcmp(str[0],"ls")==0)
        {
            show_directory(netfd,conn,username);
        }
        else if(strcmp(str[0],"gets")==0)
        {
           ret = transFile(netfd,str[1],conn,username);
           if(ret==-1) break;
        }
        else if(strcmp(str[0],"puts")==0)
        {
            ret=recvFile(netfd,conn,username);
            if(ret==-1) break;
        }
        else if(strcmp(str[0],"mkdir")==0)
        {
            make_directory(str[1],netfd,conn,username);
        }
        else if(strcmp(str[0],"rm")==0)
        {
            char *pSave;
            char *temp;
            char passname[4096]={0};
            char res[4096]={0};
            char query[4096];
            //查询pwd和id
            sprintf(query,"select pwd,id from user where username='%s'",username);
            mysql_query(conn,query);
            MYSQL_RES *result = mysql_use_result(conn);
            MYSQL_ROW row;
            row = mysql_fetch_row(result);
            strcpy(passname,row[0]);
            int uid=atoi(row[1]);
            mysql_free_result(result);
            //查询当前用户的根目录
            sprintf(query,"select id from File where filename='%s' and parent_id=0",username);
            mysql_query(conn,query);
            result = mysql_use_result(conn);
            row = mysql_fetch_row(result);
            int fid = atoi(row[0]);
            mysql_free_result(result);
            temp = __strtok_r(passname,"/",&pSave);
            strcpy(res,username);
            //查询当前目录的id
            while(temp!=NULL)
            {
                temp = __strtok_r(NULL,"/",&pSave);
                if(temp==NULL) break;
                int ret=MoveToNext(res,temp,conn,uid,&fid);
            }
            sprintf(query,"select id from File where parent_id=%d and filename='%s'",fid,str[1]);
            mysql_query(conn,query);
            result = mysql_use_result(conn);
            row = mysql_fetch_row(result);
            if(row==NULL)
            {
                report_msg(netfd,"文件或目录不存在");
                mysql_free_result(result);
            }
            else{
                fid=atoi(row[0]);
                mysql_free_result(result);
                remove_file(fid,conn);
                report_msg(netfd,"文件删除成功");
                
            }
        }
        else break;
    }
    mysql_close(conn);
    return 0;
}