#include "server_func.h"
//向netfd传输文件
int transFile(int netFd,char *filename,MYSQL *conn,char *user){
    char *pSave;
    char *temp;
    char passname[4096]={0};
    char res[4096]={0};
    char query[4096];
    //查询pwd和id
    sprintf(query,"select pwd,id from user where username='%s'",user);
    mysql_query(conn,query);
    MYSQL_RES *result = mysql_use_result(conn);
    MYSQL_ROW row;
    row = mysql_fetch_row(result);
    strcpy(passname,row[0]);
    int uid=atoi(row[1]);
    mysql_free_result(result);
    //查询当前用户的根目录
    sprintf(query,"select id from File where filename='%s' and parent_id=0",user);
    mysql_query(conn,query);
    result = mysql_use_result(conn);
    row = mysql_fetch_row(result);
    int fid = atoi(row[0]);
    mysql_free_result(result);
    temp = __strtok_r(passname,"/",&pSave);
    strcpy(res,user);
    //查询当前目录的id
    while(temp!=NULL)
    {
        temp = __strtok_r(NULL,"/",&pSave);
        if(temp==NULL) break;
        int ret=MoveToNext(res,temp,conn,uid,&fid);
    }
    //检查当前目录中文件是否存在
    sprintf(query,"select md5 from File where owner_id=%d and parent_id=%d and filename='%s' and type=0"
    ,uid,fid,filename);
    mysql_query(conn,query);
    result = mysql_use_result(conn);
    row = mysql_fetch_row(result);
    if(row==NULL)
    {
        report_msg(netFd,"文件不存在");
        mysql_free_result(result);
        return 0;
    }
    char md5[4096]={0};
    strcpy(md5,row[0]);
    mysql_free_result(result);
    train_t t;
    bzero(&t,sizeof(t));
    memcpy(t.buf,filename,strlen(filename));
    t.length = strlen(filename);
    //发送文件名
    int ret= send(netFd,&t,sizeof(int)+strlen(t.buf),MSG_NOSIGNAL);
    if(ret==-1){
        return -1;
    }
    int fd = open(md5,O_RDONLY);
    ERROR_CHECK(fd,-1,"open");
    bzero(&t,sizeof(t));
    //接受文件长度
    ret=recvn(netFd,&t.length,sizeof(int));
    if(ret==-1){
        close(fd);
        return -1;
    }
    off_t size;
    ret = recvn(netFd,t.buf,t.length);
    if(ret==-1){
        close(fd);
        return -1;
    }
    memcpy(&size,t.buf,t.length);
    lseek(fd,size,SEEK_SET);
    
    while(1){
        bzero(&t,sizeof(t));
        t.length = read(fd,t.buf,sizeof(t.buf));
        ERROR_CHECK(t.length,-1,"read");
        if(t.length != sizeof(t.buf)){
          printf("t.length = %d\n",t.length);
        }
        if(t.length == 0){
            bzero(&t,sizeof(t));
            send(netFd,&t,4,MSG_NOSIGNAL);
            break;
        }
         ret = send(netFd,&t,sizeof(int)+t.length,MSG_NOSIGNAL);
        if(ret == -1){
             perror("send");
             close(fd);
             return -1;
         }
    }
    close(fd);
    return 0;
}
int recvn(int sockFd,void *pstart,int len){
    int total = 0;
    int ret;
    char *p = (char *)pstart;
    while(total < len){
        ret = recv(sockFd,p+total,len-total,0);
        if(ret==0) return -1;
        total += ret;
    }
    return 0;
}
int recvFile(int netFd,MYSQL *conn,char *user)
{
    char *pSave;
    char *temp;
    char passname[4096]={0};
    char res[4096]={0};
    char query[4096];
    //查询pwd和id
    sprintf(query,"select pwd,id from user where username='%s'",user);
    mysql_query(conn,query);
    MYSQL_RES *result = mysql_use_result(conn);
    MYSQL_ROW row;
    row = mysql_fetch_row(result);
    strcpy(passname,row[0]);
    int uid=atoi(row[1]);
    mysql_free_result(result);
    //查询当前用户的根目录
    sprintf(query,"select id from File where filename='%s' and parent_id=0",user);
    mysql_query(conn,query);
    result = mysql_use_result(conn);
    row = mysql_fetch_row(result);
    int fid = atoi(row[0]);
    mysql_free_result(result);
    temp = __strtok_r(passname,"/",&pSave);
    strcpy(res,user);
    //查询当前目录的id
    while(temp!=NULL)
    {
        temp = __strtok_r(NULL,"/",&pSave);
        if(temp==NULL) break;
        int ret=MoveToNext(res,temp,conn,uid,&fid);
    }



    train_t t;
    bzero(&t,sizeof(t));
    int ret;
    //先接收文件名长度
    ret= recvn(netFd, &t.length, sizeof(int));
    if(ret==-1) return -1;
    //再接收文件名
    ret=recvn(netFd, t.buf, t.length);
    if(ret==-1) return -1;
    char filename[4096]={0};
    strcpy(filename,t.buf);
    //接收文件的md5码
    bzero(&t,sizeof(t));
    ret= recvn(netFd, &t.length, sizeof(int));
    if(ret==-1) return -1;
    ret=recvn(netFd, t.buf, t.length);
    if(ret==-1) return -1;
    char md5[1024]={0};
    strcpy(md5,t.buf);
    //如果数据库中能找到相同的md5码，则直接添加一条记录即可（文件秒传）
    sprintf(query,"select filesize from File where md5='%s'",md5);
    mysql_query(conn,query);
    result = mysql_use_result(conn);
    row = mysql_fetch_row(result);
    if(row!=NULL)
    {
        int filesize=atoi(row[0]);
        mysql_free_result(result);
        //查找当前目录下有无md5相同的文件,如果有就直接返回
        sprintf(query,"select id from File where md5='%s' and parent_id=%d",md5,fid);
        mysql_query(conn,query);
        result = mysql_use_result(conn);
        row = mysql_fetch_row(result);
        if(row==NULL)
        {
            mysql_free_result(result);
            sprintf(query,"insert into File (parent_id,filename,owner_id,md5,filesize,type) values(%d,'%s',%d,'%s',%d,0)"
            ,fid,filename,uid,md5,filesize);
            mysql_query(conn,query);
            result = mysql_use_result(conn);
            mysql_free_result(result);
            
        }
        else mysql_free_result(result);
        report_msg(netFd,"文件秒传成功");
        return 0;
    }
    //没有相同的md5码就开始上传文件
    mysql_free_result(result);
    report_msg(netFd,"开始上传文件");
    //接收方创建一个md5相同的文件
    int fd = open(md5, O_WRONLY | O_CREAT, 0666);
    ERROR_CHECK(fd, -1, "open");
    //发送同名文件的长度
    struct stat filestat;
    stat(md5,&filestat);
    bzero(&t,sizeof(t));
    t.length = sizeof(filestat.st_size);
    memcpy(t.buf,&filestat.st_size,sizeof(filestat.st_size));
    ret = send(netFd,&t,sizeof(int)+t.length,MSG_NOSIGNAL);
    if(ret==-1){
        close(fd);
        return -1;
    }
    lseek(fd,filestat.st_size,SEEK_SET);

    while (1)
    {
        bzero(&t,sizeof(t));
        ret=recvn(netFd,&t.length,sizeof(int));
        if(ret==-1){
            close(fd);
            return -1;
        }
        if(0 == t.length){
            break;
        }
        recvn(netFd,t.buf,t.length);
        write(fd,t.buf,t.length);
    }
    close(fd);
    stat(md5,&filestat);
    int filesize = filestat.st_size;
    sprintf(query,"insert into File (parent_id,filename,owner_id,md5,filesize,type) values(%d,'%s',%d,'%s',%d,0)"
            ,fid,filename,uid,md5,filesize);
    mysql_query(conn,query);
    result = mysql_use_result(conn);
    mysql_free_result(result);
}
//计算文件的md5码
int Compute_file_md5(const char *file_path, char *md5_str)
{
	int i;
	int fd;
	int ret;
	unsigned char data[READ_DATA_SIZE];
	unsigned char md5_value[MD5_SIZE];
	MD5_CTX md5;
 
	fd = open(file_path, O_RDONLY);
	if (-1 == fd)
	{
		perror("open");
		return -1;
	}
 
	// init md5
	MD5Init(&md5);
 
	while (1)
	{
		ret = read(fd, data, READ_DATA_SIZE);
		if (-1 == ret)
		{
			perror("read");
			return -1;
		}
 
		MD5Update(&md5, data, ret);
 
		if (0 == ret || ret < READ_DATA_SIZE)
		{
			break;
		}
	}
 
	close(fd);
 
	MD5Final(&md5, md5_value);
 
	for(i = 0; i < MD5_SIZE; i++)
	{
		snprintf(md5_str + i*2, 2+1, "%02x", md5_value[i]);
	}
	md5_str[MD5_STR_LEN] = '\0'; // add end
 
	return 0;
}