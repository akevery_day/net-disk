#include "server_func.h"
//产生一个长度为length的随机字符串
char* GenerateStr(int length)
{
    char *str = (char *)calloc(length+1,sizeof(char));
    srand(time(NULL));
    for(int i=0;i<length;i++)
    {
        int flag = rand()%3;
        switch (flag)
        {
        case 0:
            str[i]='A'+rand()%26;
            break;
        case 1:
            str[i]='a'+rand()%26;
            break;
        default:
            str[i]='0'+rand()%10;
            break;
        }
    }
    str[length]='\0';
    return str;
}
//处理客户端的注册请求
int client_register(int netfd)
{
    MYSQL *conn =NULL;
    char *host = "localhost";
    char *user = "root";
    char *password = "123456";
    char * db ="FileServer";
    conn = mysql_init(NULL);
    mysql_real_connect(conn,host,user,password,db,0,NULL,0);
    train_t msg;
    bzero(&msg,sizeof(msg));
    //接收用户名
    recvn(netfd,&msg.length,sizeof(int));
    recvn(netfd,msg.buf,msg.length);
    char username[4096]={0};
    strcpy(username,msg.buf);
    char query[4096]={0};
    sprintf(query,"select id from user where username='%s'",username);
    mysql_query(conn,query);
    MYSQL_RES *result = mysql_use_result(conn);
    
    MYSQL_ROW row;
    row = mysql_fetch_row(result);
    if(row!=NULL){
        report_msg(netfd,"用户名已存在，请重新输入");
        mysql_free_result(result);
        mysql_close(conn);
        return -1;
    }
    //随机生成盐值，并将盐值发送给客户端
    char *salt=GenerateStr(10);
    report_msg(netfd,salt);
    //接受客户端返回的加密密码
    recvn(netfd,&msg.length,sizeof(int));
    recvn(netfd,msg.buf,msg.length);
    char cryptpassword[4096]={0};
    strcpy(cryptpassword,msg.buf);
    //向数据库中插入新用户的信息
    sprintf(query,"insert into user (username,salt,cryptpassword,pwd) values ('%s','%s','%s','%s')",username,salt,cryptpassword,username);
    mysql_query(conn,query);
    //向客户端返回注册成功的消息
    int id;
    report_msg(netfd,"新用户注册成功");
    mysql_free_result(result);
    sprintf(query,"select id from user where username='%s'",username);
    mysql_query(conn,query);
    result = mysql_use_result(conn);
    row = mysql_fetch_row(result);
    id=atoi(row[0]);
    mysql_free_result(result);
    sprintf(query,"insert into File (parent_id,filename,owner_id,type) values (0,'%s',%d,1)",username,id);
    mysql_query(conn,query);
    mysql_close(conn);
    return 0;
}
//处理客户端的登录请求
int client_log(int netfd,char *name)
{
    MYSQL *conn =NULL;
    char *host = "localhost";
    char *user = "root";
    char *password = "123456";
    char * db ="FileServer";
    conn = mysql_init(NULL);
    mysql_real_connect(conn,host,user,password,db,0,NULL,0);
    train_t msg;
    bzero(&msg,sizeof(msg));
    //接收用户名
    recvn(netfd,&msg.length,sizeof(int));
    recvn(netfd,msg.buf,msg.length);
    char username[4096]={0};
    strcpy(username,msg.buf);
    char query[4096]={0};
    //向客户端发送盐值
    sprintf(query,"select salt from user where username='%s'",username);
    mysql_query(conn,query);
    MYSQL_RES *result = mysql_use_result(conn);
    MYSQL_ROW row;
    row = mysql_fetch_row(result);
    if(row==NULL)
    {
        report_msg(netfd,"用户名或密码错误");
        mysql_free_result(result);
        mysql_close(conn);
        return -1;
    }
    report_msg(netfd,row[0]);
    //接收客户端发送的加密密码
    char cryptpsw[4096]={0};
    bzero(&msg,sizeof(msg));
    recvn(netfd,&msg.length,sizeof(int));
    recvn(netfd,msg.buf,msg.length);
    strcpy(cryptpsw,msg.buf);
    //验证密码是否正确
    sprintf(query,"select cryptpassword from user where username='%s'",username);
    mysql_free_result(result);
    mysql_query(conn,query);
    
    result = mysql_use_result(conn);
    //puts(query);
    row = mysql_fetch_row(result);
    //puts(row[0]);
    if(strcmp(cryptpsw,row[0])!=0)
    {
        report_msg(netfd,"用户名或密码错误");
        mysql_free_result(result);
        mysql_close(conn);
        return -1;
    }
    report_msg(netfd,"用户登录成功");
    strcpy(name,username);
    mysql_free_result(result);
    mysql_close(conn);
    return 0;
}