#include "head.h"
#include "md5.h"
//任务结点
typedef struct task_s{
    int netFd;//传递已连接socket文件描述符
    struct task_s *pNext;
} task_t;
#define READ_DATA_SIZE	1024
#define MD5_SIZE		16
#define MD5_STR_LEN		(MD5_SIZE * 2)
//任务队列
typedef struct taskQueue_s{
    task_t *pFront;//队首指针
    task_t *pRear;//队尾指针
    int size;//队列大小
    pthread_mutex_t mutex;
    pthread_cond_t cond;
} taskQueue_t;
typedef struct threadPool_s {
    pthread_t *tid;//线程池中线程的tid
    int threadNum;//线程池中的线程数量
    taskQueue_t taskQueue;
    int exitFlag;//退出标记
} threadPool_t;
typedef struct train_s{
    int length;//要发送的数据包的长度
    char buf[4096];
} train_t;
int taskEnqueue(taskQueue_t *pTaskQueue, int netFd);
int taskDequeue(taskQueue_t *pTaskQueue);
int threadPoolInit(threadPool_t *pThreadPool, int workerNum);
int makeWorker(threadPool_t *pThreadPool);
int tcpInit(int *pSockFd, char *ip, char *port);
int epollAdd(int fd, int epfd);
int epollDel(int fd, int epfd);
int transFile(int netFd,char *filename,MYSQL *conn,char *user);
int report_msg(int netfd,char msg[]);
int interact_with_client(int netfd);
int change_directory(char *passname,int netfd,MYSQL *conn,char *user);
int present_work_directory(int netfd,MYSQL *conn,char user[]);
int show_directory(int netfd,MYSQL *conn,char user[]);
int recvn(int sockFd,void *pstart,int len);
int recvFile(int netFd,MYSQL *conn,char *user);
int make_directory(char *dirname,int netfd,MYSQL *conn,char user[]);
int remove_file(int fid,MYSQL *conn);
char* GenerateStr(int length);
int client_register(int netfd);
int client_log(int netfd,char *name);
int Compute_file_md5(const char *file_path, char *value);
int MoveToNext(char* passname,char *dirname,MYSQL *conn,int uid,int *file_id);