#include "server_func.h"
//将fd加入epfd的监听集合
int epollAdd(int fd, int epfd){
    struct epoll_event event;
    event.events = EPOLLIN;
    event.data.fd = fd;
    int ret = epoll_ctl(epfd,EPOLL_CTL_ADD,fd,&event);
    ERROR_CHECK(ret,-1,"epoll_ctl");
    return 0;
}
//将fd从epfd的监听集合中删去
int epollDel(int fd, int epfd){
    struct epoll_event event;
    event.events = EPOLLIN;
    event.data.fd = fd;
    int ret = epoll_ctl(epfd,EPOLL_CTL_DEL,fd,&event);
    ERROR_CHECK(ret,-1,"epoll_ctl");
    return 0;
}
//将一个任务加入任务队列
int taskEnqueue(taskQueue_t *pTaskQueue, int netFd){
    task_t *pTask = (task_t *)calloc(1,sizeof(task_t));
    pTask->netFd = netFd;
    if(pTaskQueue->size == 0){
        pTaskQueue->pFront = pTask;
        pTaskQueue->pRear = pTask;
    }
    else{
        pTaskQueue->pRear->pNext = pTask;
        pTaskQueue->pRear = pTask;
    }
    ++pTaskQueue->size;
    return 0;
}
//将一个任务从任务队列中出队
int taskDequeue(taskQueue_t *pTaskQueue){
    task_t *pCur = pTaskQueue->pFront;
    pTaskQueue->pFront = pCur->pNext;
    free(pCur);
    --pTaskQueue->size;
    return 0;
}
//初始化TCP连接
int tcpInit(int *pSockFd, char *ip, char *port){
    *pSockFd = socket(AF_INET,SOCK_STREAM,0);
    ERROR_CHECK(*pSockFd,-1,"socket");
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(port));
    addr.sin_addr.s_addr = inet_addr(ip);
    int reuse = 1;
    int ret = setsockopt(*pSockFd,SOL_SOCKET,SO_REUSEADDR,&reuse,sizeof(reuse));
    ERROR_CHECK(ret,-1,"setsockopt");
    ret = bind(*pSockFd,(struct sockaddr *)&addr, sizeof(addr));
    ERROR_CHECK(ret,-1,"bind");
    listen(*pSockFd,10);    
}

//资源清理栈出栈的时候释放锁
void cleanFunc(void *arg){
    threadPool_t * pThreadPool = (threadPool_t *)arg;
    pthread_mutex_unlock(&pThreadPool->taskQueue.mutex);
}
//子线程的启动函数
void * handleEvent(void *arg){
    threadPool_t * pThreadPool = (threadPool_t *)arg;
    int netFd;
    while(1){
        printf("I am free! tid = %lu\n", pthread_self());
        pthread_mutex_lock(&pThreadPool->taskQueue.mutex);
        pthread_cleanup_push(cleanFunc,(void *)pThreadPool);
        while(pThreadPool->taskQueue.size == 0 && pThreadPool->exitFlag == 0){
            pthread_cond_wait(&pThreadPool->taskQueue.cond,&pThreadPool->taskQueue.mutex);
        }
        if(pThreadPool->exitFlag != 0){
            printf("I am going to die child thread!\n");
            pthread_exit(NULL);
        }
        //从队首中取一个已连接socket的文件描述符
        netFd = pThreadPool->taskQueue.pFront->netFd;
        taskDequeue(&pThreadPool->taskQueue);
        pthread_cleanup_pop(1);
        printf("I am working! tid = %lu\n", pthread_self());
        //transFile(netFd);
        interact_with_client(netFd);//子线程在这个函数中进行实际的工作
        printf("done\n");
        close(netFd);
    }
}
//创建子线程
int makeWorker(threadPool_t *pThreadPool){
    for(int i = 0; i < pThreadPool->threadNum; ++i){
        pthread_create(&pThreadPool->tid[i],NULL,handleEvent,(void *)pThreadPool);
    }
}
//向客户端发送报告
int report_msg(int netfd,char msg[])
{
    train_t t;
    bzero(&t,sizeof(t));
    memcpy(t.buf,msg,strlen(msg));
    t.length = strlen(t.buf);
    send(netfd,&t,sizeof(int)+t.length,MSG_NOSIGNAL);
    return 0;
}