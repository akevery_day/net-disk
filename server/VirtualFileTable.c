#include "server_func.h"
//单级目录切换
int MoveToNext(char* passname,char *dirname,MYSQL *conn,int uid,int *file_id)
{
    char query[4096];
    if(strcmp(dirname,".")==0) return 0;
    else if(strcmp(dirname,"..")==0)
    {
        sprintf(query,"select parent_id from File where id=%d",*file_id);
        //printf("%d\n",*file_id);
        mysql_query(conn,query);
        MYSQL_RES *result = mysql_use_result(conn);
        MYSQL_ROW row;
        row = mysql_fetch_row(result);
        int parent_id = atoi(row[0]);
        mysql_free_result(result);
        if(parent_id==0){
            //puts("1");
            return -1;
        }

        *file_id = parent_id;
        for(int i=strlen(passname)-1;passname[i]!='/';i--)
        passname[i]='\0';
        passname[strlen(passname)-1]='\0';
        //printf("%s\n",passname);
        return 0;
    }
    //printf("%d %d %s\n",*file_id,uid,dirname);
    sprintf(query,"select id from File where parent_id=%d and owner_id=%d and filename='%s' and type=1",*file_id,uid,dirname);
    mysql_query(conn,query);
    MYSQL_RES *result = mysql_use_result(conn);
    MYSQL_ROW row;
    row = mysql_fetch_row(result);
    if(row==NULL){
        mysql_free_result(result);
        //printf("%s\n",dirname);
        return -1;
    }
    strcat(passname,"/");
    strcat(passname,dirname);
    *file_id = atoi(row[0]);
    mysql_free_result(result);
    //puts(passname);
    return 0;
}
//改变当前工作目录
int change_directory(char *passname,int netfd,MYSQL *conn,char *user)
{
    
    char *pSave;
    char *temp;
    char newpass[4096]={0};
    char res[4096]={0};
    char query[4096];
    //查询pwd和id
    sprintf(query,"select pwd,id from user where username='%s'",user);
    mysql_query(conn,query);
    MYSQL_RES *result = mysql_use_result(conn);
    MYSQL_ROW row;
    row = mysql_fetch_row(result);
    strcpy(newpass,row[0]);
    int uid=atoi(row[1]);
    mysql_free_result(result);
    strcat(newpass,"/");
    strcat(newpass,passname);
    //查询当前用户的根目录
    sprintf(query,"select id from File where filename='%s' and parent_id=0",user);
    mysql_query(conn,query);
    result = mysql_use_result(conn);
    row = mysql_fetch_row(result);
    int fid = atoi(row[0]);
    mysql_free_result(result);
    temp = __strtok_r(newpass,"/",&pSave);
    strcpy(res,user);
    while(temp!=NULL)
    {
        temp = __strtok_r(NULL,"/",&pSave);
        if(temp==NULL) break;
        int ret=MoveToNext(res,temp,conn,uid,&fid);
        if(ret==-1)
        {
            report_msg(netfd,"非法路径或目录不存在");
            return -1;
        }
    }
    sprintf(query,"update user set pwd='%s' where id=%d",res,uid);
    mysql_query(conn,query);
    report_msg(netfd,res);
    return 0;
}
//显示当前工作目录
int present_work_directory(int netfd,MYSQL *conn,char user[])
{
    char query[4096];
    sprintf(query,"select pwd from user where username='%s'",user);
    mysql_query(conn,query);
    MYSQL_RES *result = mysql_use_result(conn);
    MYSQL_ROW row;
    row = mysql_fetch_row(result);

    if(row==NULL)
    {
        report_msg(netfd,"无法获取当前工作目录");
        mysql_free_result(result);
        return -1;
    }
    char cwd[4096];
    strcpy(cwd,row[0]);
    mysql_free_result(result);
    report_msg(netfd,cwd);
    return 0;
}
//显示当前目录下的所有文件
int show_directory(int netfd,MYSQL *conn,char user[])
{
   
    char *pSave;
    char *temp;
    char passname[4096]={0};
    char res[4096]={0};
    char query[4096];
    //查询pwd和id
    sprintf(query,"select pwd,id from user where username='%s'",user);
    mysql_query(conn,query);
    MYSQL_RES *result = mysql_use_result(conn);
    MYSQL_ROW row;
    row = mysql_fetch_row(result);
    strcpy(passname,row[0]);
    int uid=atoi(row[1]);
    mysql_free_result(result);
    //查询当前用户的根目录
    sprintf(query,"select id from File where filename='%s' and parent_id=0",user);
    mysql_query(conn,query);
    result = mysql_use_result(conn);
    row = mysql_fetch_row(result);
    int fid = atoi(row[0]);
    mysql_free_result(result);
    temp = __strtok_r(passname,"/",&pSave);
    strcpy(res,user);
    //查询当前目录的id
    while(temp!=NULL)
    {
        temp = __strtok_r(NULL,"/",&pSave);
        if(temp==NULL) break;
        int ret=MoveToNext(res,temp,conn,uid,&fid);
    }
    sprintf(query,"select filename from File where parent_id=%d",fid);
    mysql_query(conn,query);
    result = mysql_use_result(conn);
    while((row = mysql_fetch_row(result))!=NULL)
    {
        report_msg(netfd,row[0]);
    }
    int len=0;
    send(netfd,&len,sizeof(int),MSG_NOSIGNAL);
    mysql_free_result(result);
    return 0;
}
//创建目录
int make_directory(char *dirname,int netfd,MYSQL *conn,char user[])
{
    int len =strlen(dirname);
    for(int i=0;i<len;i++)
    if(dirname[i]=='/')
    {
        report_msg(netfd,"目录名称非法");
        return -1;
    }
    char *pSave;
    char *temp;
    char passname[4096]={0};
    char res[4096]={0};
    char query[4096];
    //查询pwd和id
    sprintf(query,"select pwd,id from user where username='%s'",user);
    mysql_query(conn,query);
    MYSQL_RES *result = mysql_use_result(conn);
    MYSQL_ROW row;
    row = mysql_fetch_row(result);
    strcpy(passname,row[0]);
    int uid=atoi(row[1]);
    mysql_free_result(result);
    //查询当前用户的根目录
    sprintf(query,"select id from File where filename='%s' and parent_id=0",user);
    mysql_query(conn,query);
    result = mysql_use_result(conn);
    row = mysql_fetch_row(result);
    int fid = atoi(row[0]);
    mysql_free_result(result);
    temp = __strtok_r(passname,"/",&pSave);
    strcpy(res,user);
    //查询当前目录的id
    while(temp!=NULL)
    {
        temp = __strtok_r(NULL,"/",&pSave);
        if(temp==NULL) break;
        int ret=MoveToNext(res,temp,conn,uid,&fid);
    }
    sprintf(query,"select id from File where parent_id=%d and filename='%s'",fid,dirname);
    mysql_query(conn,query);
    result = mysql_use_result(conn);
    row = mysql_fetch_row(result);
    if(row!=NULL)
    {
        report_msg(netfd,"目录名称重复");
        return -1;
    }
    mysql_free_result(result);
    sprintf(query,"insert into File (parent_id,filename,owner_id,type) values (%d,'%s',%d,1)",fid,dirname,uid);
    mysql_query(conn,query);
    result = mysql_use_result(conn);
    mysql_free_result(result);
    report_msg(netfd,"目录创建成功");
    return 0;
}
//删除指定文件
int remove_file(int fid,MYSQL *conn)
{
    /*int ret = remove(filename);
    if(ret == -1){
        report_msg(netfd,"删除文件失败");
        return -1;
    }
    report_msg(netfd,"删除成功");
    return 0;*/
    char query[4096]={0};
    sprintf(query,"select type,md5 from File where id=%d",fid);
    mysql_query(conn,query);
    MYSQL_RES *result = mysql_use_result(conn);
    MYSQL_ROW row;
    row = mysql_fetch_row(result);
    int type = atoi(row[0]);
    char md5[4096]={0};
    if(type==0) strcpy(md5,row[1]);
    mysql_free_result(result);
    if(type==1){
        sprintf(query,"select id from File where parent_id=%d",fid);
        mysql_query(conn,query);
        result = mysql_use_result(conn);
        int cnt=0;
        int fileid[1024]={0};
        while((row = mysql_fetch_row(result))!=NULL)
        {
            fileid[cnt++]=atoi(row[0]);
        }
        mysql_free_result(result);
        for(int i=0;i<cnt;i++) remove_file(fileid[i],conn);
       
    }
    else{
        sprintf(query,"select count(*) from File where md5=(select md5 from File where id = %d)",fid);
        mysql_query(conn,query);
        result = mysql_use_result(conn);
        row = mysql_fetch_row(result);
        int cnt=atoi(row[0]);
        mysql_free_result(result);
        if(cnt==1) remove(md5);
    }
    sprintf(query,"delete from File where id=%d",fid);
    mysql_query(conn,query);
    result = mysql_use_result(conn);
    mysql_free_result(result);
    return 0;
}
