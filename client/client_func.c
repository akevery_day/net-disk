#include "head.h"
#include "client_func.h"
#include "md5.h"
int recvn(int sockFd,void *pstart,int len){
    int total = 0;
    int ret;
    char *p = (char *)pstart;
    while(total < len){
        ret = recv(sockFd,p+total,len-total,0);
        if(ret==0) return -1;
        total += ret;
    }
    return 0;
}
int recvFile(int netFd)
{
    train_t t;
    bzero(&t,sizeof(t));
    int ret;
//先接收文件名长度
   ret= recvn(netFd, &t.length, sizeof(int));
   if(ret==-1) return -1;
//再接收文件名
    ret=recvn(netFd, t.buf, t.length);
    if(ret==-1) return -1;
    if(strcmp(t.buf,"文件不存在")==0){
        puts(t.buf);
        return 0;
    }
//接收方创建一个同名文件
    int fd = open(t.buf, O_WRONLY | O_CREAT, 0666);
    ERROR_CHECK(fd, -1, "open");
    //发送同名文件的长度
    struct stat filestat;
    stat(t.buf,&filestat);
    bzero(&t,sizeof(t));
    t.length = sizeof(filestat.st_size);
    memcpy(t.buf,&filestat.st_size,sizeof(filestat.st_size));
    ret = send(netFd,&t,sizeof(int)+t.length,MSG_NOSIGNAL);
    if(ret==-1){
        close(fd);
        return -1;
    }
    lseek(fd,filestat.st_size,SEEK_SET);
    //int total=0;
    while (1)
    {
        bzero(&t,sizeof(t));
        ret=recvn(netFd,&t.length,sizeof(int));
        if(ret==-1){
            close(fd);
            return -1;
        }
        if(0 == t.length){
            break;
        }
        recvn(netFd,t.buf,t.length);
        write(fd,t.buf,t.length);
        /*total+=t.length;
        if(total>1500000){
            close(fd);
            return -1;
        }*/
    }
    close(fd);
    return 0;
}
int transFile(int netFd,char *filename){
    train_t t;
    bzero(&t,sizeof(t));
    memcpy(t.buf,filename,strlen(filename));
    t.length = strlen(filename);
    //发送文件名
    int ret= send(netFd,&t,sizeof(int)+strlen(t.buf),MSG_NOSIGNAL);
    if(ret==-1){
        return -1;
    }
    int fd = open(filename,O_RDONLY);
    ERROR_CHECK(fd,-1,"open");
    bzero(&t,sizeof(t));
    //发送md5码
    char md5[4096];
    Compute_file_md5(filename,md5);
    report_msg(netFd,md5);
    //接收服务端返回的消息
    bzero(&t,sizeof(t));
    ret= recvn(netFd, &t.length, sizeof(int));
    if(ret==-1) return -1;
    ret=recvn(netFd, t.buf, t.length);
    if(ret==-1) return -1;
    if(strcmp(t.buf,"文件秒传成功")==0)
    {
        close(fd);
        puts(t.buf);
        return 0;
    }
    //接受文件长度
    ret=recvn(netFd,&t.length,sizeof(int));
    if(ret==-1){
        close(fd);
        return -1;
    }
    off_t size;
    ret = recvn(netFd,t.buf,t.length);
    if(ret==-1){
        close(fd);
        return -1;
    }
    memcpy(&size,t.buf,t.length);
    lseek(fd,size,SEEK_SET);
    
    while(1){
        bzero(&t,sizeof(t));
        t.length = read(fd,t.buf,sizeof(t.buf));
        ERROR_CHECK(t.length,-1,"read");
        if(t.length != sizeof(t.buf)){
          printf("t.length = %d\n",t.length);
        }
        if(t.length == 0){
            bzero(&t,sizeof(t));
            send(netFd,&t,4,MSG_NOSIGNAL);
            break;
        }
         ret = send(netFd,&t,sizeof(int)+t.length,MSG_NOSIGNAL);
        if(ret == -1){
             perror("send");
             close(fd);
             return -1;
         }
    }
    close(fd);
    return 0;
}
//向服务端发送报告
int report_msg(int netfd,char msg[])
{
    train_t t;
    bzero(&t,sizeof(t));
    memcpy(t.buf,msg,strlen(msg));
    t.length = strlen(t.buf);
    send(netfd,&t,sizeof(int)+t.length,MSG_NOSIGNAL);
    return 0;
}
//客户端注册新用户
int client_register(int netfd)
{
    char user[4096]={0};
    char password[4096]={0};
    printf("请输入用户名:");
    fflush(stdout);
    scanf("%s",user);
    printf("请输入密码:");
    fflush(stdout);
    scanf("%s",password);
    //向服务端发送用户名
    report_msg(netfd,user);
    train_t msg;
    bzero(&msg,sizeof(msg));
    //接收服务端返回的盐值或报错信息
    recvn(netfd,&msg.length,sizeof(int));
    recvn(netfd,msg.buf,msg.length);
   
    if(strcmp(msg.buf,"用户名已存在，请重新输入")==0){
        printf("用户名已存在，请重新输入\n");
        return -1;
    }
     //return 0;
    char *cryptpasswd = crypt(password,msg.buf);
    //向服务器发送加密后的密码
    report_msg(netfd,cryptpasswd);
    bzero(&msg,sizeof(msg));
    recvn(netfd,&msg.length,sizeof(int));
    recvn(netfd,msg.buf,msg.length);
    printf("%s\n",msg.buf);
    return 0;
}
//客户端登录
int client_log(int netfd)
{
    char user[4096]={0};
    char password[4096]={0};
    printf("请输入用户名:");
    fflush(stdout);
    scanf("%s",user);
    printf("请输入密码:");
    fflush(stdout);
    scanf("%s",password);
    //向服务端发送用户名
    report_msg(netfd,user);
    train_t msg;
    bzero(&msg,sizeof(msg));
    //接收服务端返回的盐值或报错信息
    recvn(netfd,&msg.length,sizeof(int));
    recvn(netfd,msg.buf,msg.length);
   
    if(strcmp(msg.buf,"用户名或密码错误")==0){
        printf("用户名或密码错误\n");
        return -1;
    }
    
    char *cryptpasswd = crypt(password,msg.buf);
    //向服务器发送加密后的密码
    report_msg(netfd,cryptpasswd);
    bzero(&msg,sizeof(msg));
    recvn(netfd,&msg.length,sizeof(int));
    recvn(netfd,msg.buf,msg.length);

    printf("%s\n",msg.buf);
    if(strcmp(msg.buf,"用户名或密码错误")==0){
        return -1;
    }
    return 0;
}
//计算文件的md5码
int Compute_file_md5(const char *file_path, char *md5_str)
{
	int i;
	int fd;
	int ret;
	unsigned char data[READ_DATA_SIZE];
	unsigned char md5_value[MD5_SIZE];
	MD5_CTX md5;
 
	fd = open(file_path, O_RDONLY);
	if (-1 == fd)
	{
		perror("open");
		return -1;
	}
 
	// init md5
	MD5Init(&md5);
 
	while (1)
	{
		ret = read(fd, data, READ_DATA_SIZE);
		if (-1 == ret)
		{
			perror("read");
			return -1;
		}
 
		MD5Update(&md5, data, ret);
 
		if (0 == ret || ret < READ_DATA_SIZE)
		{
			break;
		}
	}
 
	close(fd);
 
	MD5Final(&md5, md5_value);
 
	for(i = 0; i < MD5_SIZE; i++)
	{
		snprintf(md5_str + i*2, 2+1, "%02x", md5_value[i]);
	}
	md5_str[MD5_STR_LEN] = '\0'; // add end
 
	return 0;
}