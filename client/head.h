#include<stdio.h>
#include<string.h>
#include<strings.h>
#include<stdlib.h>
#include<signal.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<fcntl.h>
#include<unistd.h>
#include<dirent.h>
#include<pwd.h>
#include<grp.h>
#include<time.h>
#include<sys/mman.h>
#include<sys/wait.h>
#include<sys/select.h>
#include<sys/time.h>
#include<syslog.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<sys/sem.h>
#include<sys/msg.h>
#include<pthread.h>
#include<arpa/inet.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>
#include<sys/epoll.h>
#include<sys/uio.h>
#include<mysql/mysql.h>
#include<shadow.h>
#include<crypt.h>
#include<errno.h>
#define ARGS_CHECK(argc,num) {if(argc!=num){fprintf(stderr,"args error\n");return -1;}}
#define ERROR_CHECK(ret,num,msg) {if(ret==num){perror(msg);return -1;}}
#define THREAD_ERROR_CHECK(ret,msg) {if(ret!=0){fprintf(stderr,"%s:%s\n",msg,strerror(ret));}}