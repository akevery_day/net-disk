#include "head.h"
#include "client_func.h"
int main(int argc, char *argv[]){
    // ./client 192.168.152.130 1234
    ARGS_CHECK(argc,3);

    int sockFd = socket(AF_INET,SOCK_STREAM,0);
    ERROR_CHECK(sockFd,-1,"socket");
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(argv[2]));
    addr.sin_addr.s_addr = inet_addr(argv[1]);
    int ret = connect(sockFd,(struct sockaddr *)&addr,sizeof(addr));
    ERROR_CHECK(ret,-1,"connect");

    train_t msg;
    bzero(&msg,sizeof(msg));
    printf("输入0:注册,输入1:登录,输入2:退出\n");
    scanf("%d",&msg.length);
    send(sockFd,&msg.length,sizeof(int),MSG_NOSIGNAL);
    
    switch (msg.length)
    {
    case 0:
        ret = client_register(sockFd);
        if(ret==-1){
            close(sockFd);
            return -1;
        }
        printf("请登录\n");
        ret = client_log(sockFd);
        if(ret==-1){
            close(sockFd);
            return -1;
        }
        break;
    case 1:
        ret = client_log(sockFd);
        if(ret==-1){
            close(sockFd);
            return -1;
        }
        break;
    default:
        close(sockFd);
        return 0;
        break;
    }
   

    char buf[4096]={0};
    chdir("download");
    while(1)
    {
        printf("请输入命令：");
        fflush(stdout);
        bzero(buf,sizeof(buf));
        int ret=read(STDIN_FILENO,buf,sizeof(buf));
        if(ret ==0) break;
        buf[strlen(buf)-1]='\0';
        bzero(&msg,sizeof(msg));
        memcpy(msg.buf,buf,strlen(buf));
        msg.length= strlen(msg.buf);
        char* arr[2];
        arr[0] = strtok(buf," ");
        arr[1] = strtok(NULL," ");
        if(strcmp(arr[0],"gets")==0)
        {
            ret = send(sockFd,&msg,sizeof(int)+msg.length,MSG_NOSIGNAL);
            if(ret<0) break;
            //chdir("download");
            ret=recvFile(sockFd);
            //chdir("..");
            if(ret==-1){
                printf("接收异常终止\n");
                break;
            }
        }
        else if(strcmp(arr[0],"puts")==0)
        {
            ret = send(sockFd,&msg,sizeof(int)+msg.length,MSG_NOSIGNAL);
            if(ret<0) break;
            //chdir("download");
            ret=transFile(sockFd,arr[1]);
            //chdir("..");
            if(ret==-1)
            {
                printf("发送异常终止\n");
                break;
            }
        }
        else if(strcmp(arr[0],"cd")==0||strcmp(arr[0],"pwd")==0||
        strcmp(arr[0],"mkdir")==0||strcmp(arr[0],"rm")==0)
        {
            ret = send(sockFd,&msg,sizeof(int)+msg.length,MSG_NOSIGNAL);
            if(ret<0) break;
            bzero(&msg,sizeof(msg));
            recvn(sockFd,&msg.length,sizeof(int));
            recvn(sockFd,msg.buf,msg.length);
            printf("%s\n",msg.buf);
        }
        else if(strcmp(arr[0],"ls")==0)
        {
            ret = send(sockFd,&msg,sizeof(int)+msg.length,MSG_NOSIGNAL);
            if(ret<0) break;
            while(1)
            {
                bzero(&msg,sizeof(msg));
                recvn(sockFd,&msg.length,sizeof(int));
                if(msg.length==0) break;
                recvn(sockFd,msg.buf,msg.length);
                printf("%s ",msg.buf);
            }
            printf("\n");
        }
        else{
            printf("非法命令\n");
        }
    } 
    msg.length = 0;
    send(sockFd,&msg.length,sizeof(int),0);
    close(sockFd);
}