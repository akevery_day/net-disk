typedef struct train_s{
    int length;
    char buf[4096];
} train_t;
#define READ_DATA_SIZE	1024
#define MD5_SIZE		16
#define MD5_STR_LEN		(MD5_SIZE * 2)
int recvn(int sockFd,void *pstart,int len);
int recvFile(int netFd);
int transFile(int netFd,char *filename);
int report_msg(int netfd,char msg[]);
int client_register(int netfd);
int client_log(int netfd);
int Compute_file_md5(const char *file_path, char *md5_str);